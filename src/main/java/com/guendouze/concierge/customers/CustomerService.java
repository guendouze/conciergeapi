package com.guendouze.concierge.customers;

import com.guendouze.concierge.errors.ConflictException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 27/01/2020 12:27.
 */

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public List<Customer> findAll() {
        List<Customer> customers = new ArrayList<>();
        customerRepository.findAll().forEach(customers::add);
        return customers;
    }

    public Optional<Customer> findBy(long id) {
        return customerRepository.findById(id);
    }

    public Customer save(Customer customer) {
        try {
            return customerRepository.save(customer);
        } catch (Exception e) {
            throw new ConflictException("Another record with the same title exists");
        }
    }


    public void delete(long id) {
        customerRepository.deleteById(id);
    }

}
