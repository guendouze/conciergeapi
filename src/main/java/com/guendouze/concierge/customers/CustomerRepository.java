package com.guendouze.concierge.customers;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 27/01/2020 12:30.
 */

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
