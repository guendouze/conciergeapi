package com.guendouze.concierge.customers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 27/01/2020 12:24.
 */

@RestController
@RequestMapping(value = "/api/v1/concierge")
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @GetMapping("/customers")
    public ResponseEntity<List<Customer>> getAllCUstomers() {
        List<Customer> customers = customerService.findAll();
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

    @GetMapping("/customers/{id}")
    public ResponseEntity<Customer> getCustomer(@PathVariable long id) {
        Optional<Customer> optionalCustomer = customerService.findBy(id);
        return (optionalCustomer.isPresent()) ? new ResponseEntity<>(optionalCustomer.get(), HttpStatus.OK) : new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }

    @PostMapping("/customers")
    public ResponseEntity<Customer> createCustomer(@Valid @RequestBody Customer customer) {
        Customer newCustomer = customerService.save(customer);
        return (newCustomer != null) ? new ResponseEntity<>(newCustomer, HttpStatus.CREATED) : new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/customers")
    public ResponseEntity<Customer> update(@RequestBody Customer customer) {
        Customer newCustomer = customerService.save(customer);
        return (newCustomer != null) ? new ResponseEntity<>(newCustomer, HttpStatus.OK) : new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping("/customers/{id}")
    public ResponseEntity<Void> delete(@PathVariable long id) {
        customerService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
