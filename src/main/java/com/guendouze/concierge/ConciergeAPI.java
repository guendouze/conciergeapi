package com.guendouze.concierge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 26/01/2020 11:52.
 */

@SpringBootApplication
@EnableJpaAuditing
public class ConciergeAPI {
    public static void main(String[] args) {
        SpringApplication.run(ConciergeAPI.class, args);
    }

}
