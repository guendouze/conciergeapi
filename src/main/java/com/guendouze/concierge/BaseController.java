package com.guendouze.concierge;

import com.guendouze.concierge.security.AppUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 03/02/2020 13:25.
 */

public abstract class BaseController {
    public AppUser getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (AppUser) authentication.getPrincipal();
    }
}
