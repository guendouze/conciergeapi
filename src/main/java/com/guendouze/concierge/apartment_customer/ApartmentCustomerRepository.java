package com.guendouze.concierge.apartment_customer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApartmentCustomerRepository extends JpaRepository<ApartmentCustomer, Long> {
}
