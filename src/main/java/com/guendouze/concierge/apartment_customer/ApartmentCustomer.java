package com.guendouze.concierge.apartment_customer;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 30/01/2020 10:58.
 */


@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity(name = "apartment_customer")
public class ApartmentCustomer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private long apartment_id;
    private long customer_id;

    private String prestations;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy hh:mm")
    private Date startDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy hh:mm")
    private Date endDate;
}
