package com.guendouze.concierge.apartment_customer;

import com.guendouze.concierge.errors.ConflictException;
import com.guendouze.concierge.errors.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 26/01/2020 20:40.
 */

@Service
public class ApartmentCustomerService {

    @Autowired
    ApartmentCustomerRepository apartmentCustomerRepository;

    public List<ApartmentCustomer> findAll() {
        List<ApartmentCustomer> apartmentCustomers = new ArrayList<>();
        apartmentCustomerRepository.findAll().forEach(apartmentCustomers::add);
        return apartmentCustomers;
    }

    public Optional<ApartmentCustomer> findBy(long id) {
        Optional<ApartmentCustomer> optionalApartmentCustomer = apartmentCustomerRepository.findById(id);
        if (optionalApartmentCustomer.isEmpty()) {
            throw new NotFoundException("reservation not found");
        }
        return optionalApartmentCustomer;
    }

    public ApartmentCustomer save(ApartmentCustomer apartmentCustomer) {
        try {
            return apartmentCustomerRepository.save(apartmentCustomer);
        } catch (Exception e) {
            throw new ConflictException("Another record with the same id exists");
        }
    }


    public ApartmentCustomer update(ApartmentCustomer apartmentCustomer) {
        return apartmentCustomerRepository.save(apartmentCustomer);
    }

    public void delete(long id) {
        apartmentCustomerRepository.deleteById(id);
    }

}
