package com.guendouze.concierge.apartment_customer;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 26/01/2020 12:00.
 */

@RestController
@RequestMapping(value = "/api/v1/concierge")
public class ApartmentCustomerController {
    private final Log logger = LogFactory.getLog(ApartmentCustomerController.class);

    @Autowired
    ApartmentCustomerService apartmentCustomerService;

    @PostMapping("/apartment-customer")
    public ResponseEntity<ApartmentCustomer> createIntervention(@Valid @RequestBody ApartmentCustomer apartmentCustomer) {
        ApartmentCustomer newApartmentCustomer = apartmentCustomerService.save(apartmentCustomer);
        return (newApartmentCustomer != null) ? new ResponseEntity<>(newApartmentCustomer, HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/apartment-customer")
    public ResponseEntity<List<ApartmentCustomer>> getAll() {
        List<ApartmentCustomer> apartmentsCustomers = apartmentCustomerService.findAll();
        return new ResponseEntity<>(apartmentsCustomers, HttpStatus.OK);
    }

    @GetMapping("/apartment-customer/{id}")
    public ResponseEntity<ApartmentCustomer> get(@Valid @PathVariable long id) {
        Optional<ApartmentCustomer> optionalApartmentCustomer = apartmentCustomerService.findBy(id);
        return (optionalApartmentCustomer.isPresent()) ? new ResponseEntity<>(optionalApartmentCustomer.get(), HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @PutMapping("/apartment-customer")
    public ResponseEntity<ApartmentCustomer> update(@RequestBody ApartmentCustomer apartmentCustomer) {
        ApartmentCustomer newApartmentCustomer = apartmentCustomerService.update(apartmentCustomer);
        return (newApartmentCustomer != null) ? new ResponseEntity<>(newApartmentCustomer, HttpStatus.OK) : new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping("/apartment-customer/{id}")
    public ResponseEntity<Void> delete(@PathVariable long id) {
        apartmentCustomerService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
