package com.guendouze.concierge.apartments;

import com.guendouze.concierge.buildings.Building;
import com.guendouze.concierge.owners.Owner;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 27/01/2020 15:26.
 */

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/api/v1/concierge")
public class ApartmentController {
    private final Log logger = LogFactory.getLog(ApartmentController.class);

    @Autowired
    ApartmentService apartmentService;

    /***************************
     *  Apartements by building
     ***************************/
    @GetMapping("/buildings/{buildingId}/apartments")
    public ResponseEntity<List<Apartment>> getAllApartmentsByBuilding(@PathVariable long buildingId) {
        List<Apartment> apartments = apartmentService.findAllApartmentsByBuilding(buildingId);
        return new ResponseEntity<>(apartments, HttpStatus.OK);
    }

    @GetMapping("/buildings/{buildingId}/apartments/{id}")
    public ResponseEntity<Apartment> getApartmentInBuilding(@PathVariable long buildingId, @PathVariable long id) {
        Optional<Apartment> optionalApartment = apartmentService.findApartementByBuilding(buildingId, id);
        return (optionalApartment.isPresent()) ? new ResponseEntity<>(optionalApartment.get(), HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // BE CAREFUL - Delete all apartments in the building
    @DeleteMapping("/buildings/{buildingId}/apartments")
    public ResponseEntity<Void> deleteAllApartementsByBuildingId(@PathVariable long buildingId) {
        apartmentService.deleteAllApartmentsByBuilding(buildingId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // BE CAREFUL - Delete one apartment in the building
    @DeleteMapping("/buildings/{buildingId}/apartments/{id}")
    public ResponseEntity<Void> deleteApartementByBuilding(@PathVariable long buildingId, @PathVariable long apartmentid) {
        apartmentService.deleteApartmentByBuilding(buildingId, apartmentid);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /***************************
     *  Apartements by owner
     ***************************/
    @GetMapping("/owners/{ownerID}/apartments")
    public ResponseEntity<List<Apartment>> getAllApartmentsByOwner(@PathVariable long ownerID) {
        logger.info("------------------------------------------------ownerID: " + ownerID);
        List<Apartment> apartments = apartmentService.allApartmentsByOwner(ownerID);
        return new ResponseEntity<>(apartments, HttpStatus.OK);
    }

    @GetMapping("/owners/{ownerId}/apartments/{id}")
    public ResponseEntity<Apartment> getApartmentByOwnerAndApartmentId(@PathVariable long ownerID, @PathVariable long apartementID) {
        Optional<Apartment> optionalApartment = apartmentService.findApartementByOwner(ownerID, apartementID);
        return (optionalApartment.isPresent()) ? new ResponseEntity<>(optionalApartment.get(), HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // BE CAREFUL - Delete all owner's apartments
    @DeleteMapping("/owners/{ownerID}/apartments")
    public ResponseEntity<Void> deleteAllApartmentsByOwner(@PathVariable long ownerID) {
        apartmentService.deleteAllApartmentsByOwner(ownerID);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    // BE CAREFUL - Delete an owner's apartment
    @DeleteMapping("/owners/{ownerID}/apartments/{id}")
    public ResponseEntity<Void> deleteOneApartmentByOwner(@PathVariable long ownerID, @PathVariable long apartmentID) {
        apartmentService.deleteOneApartmentByOwner(ownerID, apartmentID);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    /***************************
     *  Create and update apartement by building, owner
     ***************************/
    @PostMapping("/owners/{ownerID}/buildings/{buildingID}/apartments")
    public ResponseEntity<Apartment> createAprtmentInBuilding(@RequestBody Apartment apartment, @PathVariable long ownerID, @PathVariable long buildingID) {
        apartment.setOwner(new Owner(ownerID));
        apartment.setBuilding(new Building(buildingID));
        Apartment newApartment = apartmentService.save(apartment);
        return (newApartment != null) ? new ResponseEntity<>(newApartment, HttpStatus.CREATED) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/owners/{ownerID}/buildings/{buildingID}/apartments/{id}")
    public ResponseEntity<Apartment> updateAprtmentByBuilding(@RequestBody Apartment apartment, @PathVariable long ownerID, @PathVariable long buildingID, @PathVariable long id) {
        //TODO - Reflection -   Test or not the buildingId if equal Aprtment get building id
        apartment.setOwner(new Owner(ownerID));
        apartment.setBuilding(new Building(buildingID));

        Apartment newApartment = apartmentService.update(apartment);
        return (newApartment != null) ? new ResponseEntity<>(newApartment, HttpStatus.OK) : new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
    }

}
