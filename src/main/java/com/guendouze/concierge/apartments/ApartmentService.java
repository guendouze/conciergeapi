package com.guendouze.concierge.apartments;

import com.guendouze.concierge.errors.ConflictException;
import com.guendouze.concierge.errors.NotFoundException;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 27/01/2020 15:27.
 */

@Service
public class ApartmentService {
    private final Log logger = LogFactory.getLog(ApartmentService.class);

    @Autowired
    ApartmentRepository apartmentRepository;

    /***************************
     *  Apartements by building
     ***************************/
    public List<Apartment> findAllApartmentsByBuilding(long buildingID) {
        List<Apartment> apartments = new ArrayList<>();
        apartmentRepository.findByBuildingId(buildingID).forEach(apartments::add);
        return apartments;
    }

    public Optional<Apartment> findApartementByBuilding(long buildingID, long apartmentID) {
        Optional<Apartment> optionalApartment = apartmentRepository.findByBuildingIdAndId(buildingID, apartmentID);
        if (optionalApartment.isEmpty()) {
            throw new NotFoundException("Apartment not found");
        }
        return optionalApartment;
    }

    // Delete all apartments in the building
    public void deleteAllApartmentsByBuilding(long buildingID) {
        apartmentRepository.deleteByBuildingId(buildingID);
    }

    // Delete one apartment in the building
    public void deleteApartmentByBuilding(long buildingID, long apartmentID) {
        apartmentRepository.deleteByBuildingIdAndId(buildingID, apartmentID);
    }

    /***************************
     *  Apartements by owner
     ***************************/
    public List<Apartment> allApartmentsByOwner(long ownerID) {
        List<Apartment> apartments = new ArrayList<>();
        try {
            apartmentRepository.findByOwnerId(ownerID).forEach(apartments::add);
        } catch (Exception e) {
            logger.info("-----Srevice > exception: " + e.getMessage());
        }

        return apartments;
    }

    public Optional<Apartment> findApartementByOwner(long ownerID, long apartementID) {
        Optional<Apartment> optionalApartment = apartmentRepository.findByOwnerIdAndId(ownerID, apartementID);
        if (optionalApartment.isEmpty()) {
            throw new NotFoundException("Apartment not found for this owner");
        }
        return optionalApartment;
    }

    // Delete all apartments for this owner
    public void deleteAllApartmentsByOwner(long ownerID) {
        apartmentRepository.deleteByOwnerId(ownerID);
    }

    // Delete one apartment for this owner
    public void deleteOneApartmentByOwner(long ownerID, long apartmentID) {
        apartmentRepository.deleteByOwnerIdAndId(ownerID, apartmentID);
    }

    /****************************************************
     *  create apartment by building with owner
     ****************************************************/
    public Apartment save(Apartment apartment) {
        try {
            return apartmentRepository.save(apartment);
        } catch (Exception e) {
            throw new ConflictException("Another record with the same id exists");
        }
    }

    public Apartment update(Apartment apartment) {
        return apartmentRepository.save(apartment);
    }
}
