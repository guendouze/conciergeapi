package com.guendouze.concierge.apartments;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ApartmentRepository extends JpaRepository<Apartment, Long> {
    List<Apartment> findByBuildingId(long buildingID);

    List<Apartment> findByOwnerId(long ownerID);

    Optional<Apartment> findByBuildingIdAndId(long buildingID, long apartmentID);

    Optional<Apartment> findByOwnerIdAndId(long buildingID, long apartmentID);

    void deleteByBuildingId(long buildingID);

    void deleteByBuildingIdAndId(long buildingID, long apartmentID);

    void deleteByOwnerId(long ownerID);

    void deleteByOwnerIdAndId(long ownerID, long apartmentID);
}
