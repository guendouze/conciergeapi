package com.guendouze.concierge.apartments;

import com.guendouze.concierge.buildings.Building;
import com.guendouze.concierge.customers.Customer;
import com.guendouze.concierge.owners.Owner;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 27/01/2020 11:45.
 */

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "apartments")
public class Apartment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String homeAccessCode;
    private String homePhone;
    private int homeNumber;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "apartment_customer",
            joinColumns = {@JoinColumn(name = "apartment_id")},
            inverseJoinColumns = {@JoinColumn(name = "customer_id")})
    private Collection<Customer> customers;

    @ManyToOne
    private Building building;

    @ManyToOne
    private Owner owner;

}
