package com.guendouze.concierge.bills;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 30/01/2020 13:51.
 */

@RestController
@RequestMapping(value = "/api/v1/concierge")
public class BillingController {

    private final Log logger = LogFactory.getLog(BillingService.class);

    @Autowired
    BillingService billingService;

    @PostMapping("/billing")
    public ResponseEntity<Bill> create(@Valid @RequestBody Bill bill) {
        logger.info(bill);
        Bill newBill = billingService.save(bill);
        return (newBill != null) ? new ResponseEntity<>(newBill, HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @GetMapping("/billing")
    public ResponseEntity<List<Bill>> getAll() {
        List<Bill> bills = billingService.findAll();
        return new ResponseEntity<>(bills, HttpStatus.OK);
    }

    @GetMapping("/billing/{id}")
    public ResponseEntity<Bill> getBy(@PathVariable long id) {
        Optional<Bill> optionalBill = billingService.findBy(id);
        return (optionalBill.isPresent()) ? new ResponseEntity<>(optionalBill.get(), HttpStatus.OK) : new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }


    @PutMapping("/billing")
    public ResponseEntity<Bill> update(@RequestBody Bill bill) {
        Bill newBill = billingService.save(bill);
        return (newBill != null) ? new ResponseEntity<>(newBill, HttpStatus.OK) : new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping("/billing/{id}")
    public ResponseEntity<Void> delete(@PathVariable long id) {
        billingService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
