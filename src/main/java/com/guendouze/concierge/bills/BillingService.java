package com.guendouze.concierge.bills;

import com.guendouze.concierge.customers.Customer;
import com.guendouze.concierge.errors.ConflictException;
import com.guendouze.concierge.errors.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 30/01/2020 13:55.
 */

@Service
public class BillingService {

    @Autowired
    private BillingRepository billingRepository;

    public List<Bill> findAll() {
        List<Bill> bills = new ArrayList<>();
        billingRepository.findAll().forEach(bills::add);
        return bills;
    }

    public Optional<Bill> findBy(long id) {
        try {
            return billingRepository.findById(id);
        } catch (Exception e) {
            throw new NotFoundException("Bill not found : " + e.getMessage());
        }
    }

    public Bill save(Bill bill) {
        try {
            return billingRepository.save(bill);
        } catch (Exception e) {
            throw new ConflictException("Confilict: " + e.getMessage());
        }
    }

    public void delete(long id) {
        billingRepository.deleteById(id);
    }

}
