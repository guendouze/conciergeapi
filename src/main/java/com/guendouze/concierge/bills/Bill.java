package com.guendouze.concierge.bills;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.guendouze.concierge.apartment_customer.ApartmentCustomer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 28/01/2020 14:15.
 */

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "bills")
public class Bill {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "apartment_customer_id", unique = true)
    private ApartmentCustomer apartmentCustomer;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy hh:mm:ss")
    @CreationTimestamp
    private Date createdAt;

    private double price;
    private String prestations;

}
