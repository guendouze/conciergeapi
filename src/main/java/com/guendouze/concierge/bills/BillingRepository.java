package com.guendouze.concierge.bills;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 30/01/2020 13:57.
 */

@Repository
public interface BillingRepository extends JpaRepository<Bill, Long> {

}
