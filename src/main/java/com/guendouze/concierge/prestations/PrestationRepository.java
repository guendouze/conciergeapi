package com.guendouze.concierge.prestations;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 27/01/2020 17:20.
 */

@Repository
public interface PrestationRepository extends JpaRepository<Prestation, Long> {
}
