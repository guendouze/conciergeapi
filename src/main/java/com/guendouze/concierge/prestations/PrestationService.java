package com.guendouze.concierge.prestations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 27/01/2020 17:16.
 */

@Service
public class PrestationService {

    @Autowired
    private PrestationRepository prestationRepository;

    public List<Prestation> findAll() {
        List<Prestation> prestations = new ArrayList<>();
        prestationRepository.findAll().forEach(prestations::add);
        return prestations;
    }

    public Optional<Prestation> findBy(long id) {
        return prestationRepository.findById(id);
    }

    public Prestation save(Prestation prestation) {
        return prestationRepository.save(prestation);
    }

    public void delete(long id) {
        prestationRepository.deleteById(id);
    }

    public Prestation update(long id, Prestation prestation) {
            return prestationRepository.save(prestation);
    }
}
