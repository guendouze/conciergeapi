package com.guendouze.concierge.prestations;

import com.guendouze.concierge.owners.OwnerController;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 27/01/2020 17:14.
 */

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/api/v1/concierge")
public class PrestationController {
    private final Log logger = LogFactory.getLog(PrestationController.class);

    @Autowired
    private PrestationService prestationService;

    @GetMapping("/prestations")
    public ResponseEntity<List<Prestation>> getAll() {
        List<Prestation> prestations = prestationService.findAll();
        return new ResponseEntity<>(prestations, HttpStatus.OK);
    }

    @GetMapping("/prestations/{id}")
    public ResponseEntity<Prestation> getPrestation(@Valid @PathVariable long id) {
        Optional<Prestation> optionalPrestation = prestationService.findBy(id);
        return (optionalPrestation.isPresent()) ? new ResponseEntity<>(optionalPrestation.get(), HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/prestations")
    public ResponseEntity<Prestation> create(@Valid @RequestBody Prestation prestation) {
        Prestation newPrestation = prestationService.save(prestation);
        return (newPrestation != null) ? new ResponseEntity<>(newPrestation, HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/prestations/{id}")
    public ResponseEntity<Prestation> update(@Valid @RequestBody Prestation prestation,@PathVariable long id) {
        Prestation newPrestation = prestationService.update(id,prestation);
        return (newPrestation != null) ? new ResponseEntity<>(newPrestation, HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/prestations/{id}")
    public ResponseEntity<Void> delete(@PathVariable long id) {
        prestationService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
