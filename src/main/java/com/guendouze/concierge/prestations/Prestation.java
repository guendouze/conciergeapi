package com.guendouze.concierge.prestations;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 27/01/2020 17:10.
 */

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "prestations")
public class Prestation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    private double price;

    public Prestation(String name, double price) {
        this.name = name;
        this.price = price;
    }

}
