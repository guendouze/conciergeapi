package com.guendouze.concierge.security;

import lombok.*;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 03/02/2020 13:05.
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class JwtResponse {
    private String token;
}
