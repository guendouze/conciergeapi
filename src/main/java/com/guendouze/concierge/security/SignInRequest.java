package com.guendouze.concierge.security;

import lombok.*;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 03/02/2020 13:07.
 */


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SignInRequest {

    private String username;
    private String password;

}
