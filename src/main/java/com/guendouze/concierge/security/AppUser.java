package com.guendouze.concierge.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import java.util.Collection;
import java.util.Date;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 03/02/2020 11:57.
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "users")
public class AppUser implements UserDetails {
    @Id
    private long id;

    @NotEmpty
    private String email;

    @NotEmpty
    private String name;

    @NotEmpty
    @JsonIgnore
    private String password;

    private Date created;

    public AppUser(@NotEmpty String email, @NotEmpty String password, @NotEmpty String name) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.created = new Date();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
