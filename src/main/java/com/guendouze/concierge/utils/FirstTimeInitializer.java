package com.guendouze.concierge.utils;

import com.guendouze.concierge.prestations.Prestation;
import com.guendouze.concierge.prestations.PrestationService;
import com.guendouze.concierge.security.AppUser;
import com.guendouze.concierge.security.UserService;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 27/01/2020 17:05.
 */

@Component
public class FirstTimeInitializer implements CommandLineRunner {

    private final Log logger = LogFactory.getLog(FirstTimeInitializer.class);

    @Autowired
    private PrestationService prestationService;

    @Autowired
    private UserService userService;

    @Override
    public void run(String... strings) throws Exception {
        logger.info("===============================================================================================================");
        if (prestationService.findAll().isEmpty()) {
            logger.info("No Services found. Creating some services");
            List<Prestation> prestations = new ArrayList<>();
            prestations.add(new Prestation("Remise des clés", 10));
            prestations.add(new Prestation("Vérification de l'état", 20));
            prestations.add(new Prestation("Mini ménage", 25));
            prestations.add(new Prestation("Gros ménage", 50));
            prestations.forEach(prestationService::save);
        }

        if (userService.findAll().isEmpty()) {
            logger.info("No Users accounts found. Creating some users");

            AppUser user = new AppUser("guendouze@gmail.com", "demo", "demo");
            userService.save(user);
        }

    }
}
