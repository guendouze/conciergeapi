package com.guendouze.concierge.errors;

import org.springframework.http.HttpStatus;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 29/01/2020 11:07.
 */

public abstract class ApiBaseException extends RuntimeException {

    public ApiBaseException(String message) {
        super(message);
    }
    public abstract HttpStatus getStatusCode();

}
