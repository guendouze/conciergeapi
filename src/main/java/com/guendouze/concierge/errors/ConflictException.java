package com.guendouze.concierge.errors;

import org.springframework.http.HttpStatus;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 30/01/2020 14:48.
 */

public class ConflictException extends ApiBaseException {

    public ConflictException(String message) {
        super(message);
    }

    @Override
    public HttpStatus getStatusCode() {
        return HttpStatus.CONFLICT;
    }
}
