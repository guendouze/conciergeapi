package com.guendouze.concierge.errors;

import org.springframework.http.HttpStatus;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 29/01/2020 11:11.
 */

public class NotFoundException extends ApiBaseException {
    public NotFoundException(String message) {
        super(message);
    }

    @Override
    public HttpStatus getStatusCode() {
        return HttpStatus.NOT_FOUND;
    }
}
