package com.guendouze.concierge.buildings;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 02/02/2020 17:08.
 */

@Repository
public interface BuildingRepository extends JpaRepository<Building, Long> {
}
