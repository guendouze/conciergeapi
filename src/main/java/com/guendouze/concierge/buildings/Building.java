package com.guendouze.concierge.buildings;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 02/02/2020 16:59.
 */

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "builidngs")
public class Building {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "Name is required")
    @Size(min = 2, max = 20, message = "Name must be between 2 and 20 characters")
    private String name;

    @NotNull(message = "Access code is required")
    @Size(min = 4, max = 6, message = "Access code must be between 4 and 6 characters")
    private String accessCode;

    @NotNull(message = "Access code is required")
    private String number;

    @NotNull(message = "Access code is required")
    private String street;

    @NotNull(message = "Access code is required")
    private String city;

    @NotNull(message = "Access code is required")
    private String zipCode;

    @NotNull(message = "Access code is required")
    private String country;

    private double latitude;

    private double longitude;

    public Building(long id) {
        this.id = id;
    }
}
