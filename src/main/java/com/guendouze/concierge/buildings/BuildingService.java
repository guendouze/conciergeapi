package com.guendouze.concierge.buildings;

import com.guendouze.concierge.errors.ConflictException;
import com.guendouze.concierge.errors.NotFoundException;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 02/02/2020 17:06.
 */

@Service
public class BuildingService {
    private final Log logger = LogFactory.getLog(BuildingService.class);

    @Autowired
    BuildingRepository buildingRepository;

    public List<Building> findAll() {
        List<Building> buildings = new ArrayList<>();
        buildingRepository.findAll().forEach(buildings::add);
        return buildings;
    }

    public Optional<Building> findBy(long id) {
        Optional<Building> optionalBuilding = buildingRepository.findById(id);
        if (optionalBuilding.isEmpty()) {
            throw new NotFoundException("Building not found");
        }
        return optionalBuilding;
    }

    public Building save(Building building) {
        try {
            return buildingRepository.save(building);
        } catch (Exception e) {
            throw new ConflictException("Another record with the same id exists");
        }
    }

    public void delete(long id) {
        try {
            buildingRepository.deleteById(id);
        } catch (Exception e) {
            logger.error("Exception: " + e.getMessage());
        }
    }

    public Building update(Building building) {
        return buildingRepository.save(building);
    }

}
