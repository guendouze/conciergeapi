package com.guendouze.concierge.buildings;

import com.guendouze.concierge.BaseController;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 02/02/2020 17:05.
 */

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/api/v1/concierge")
public class BuildingController extends BaseController {
    private final Log logger = LogFactory.getLog(BuildingController.class);

    @Autowired
    BuildingService buildingService;

    @GetMapping("/buildings")
    public ResponseEntity<List<Building>> getAllBuildings() {
        List<Building> buildings = buildingService.findAll();
        return new ResponseEntity<>(buildings, HttpStatus.OK);
    }

    @GetMapping("/buildings/{id}")
    public ResponseEntity<Building> getBuilding(@PathVariable long id) {
        Optional<Building> optionalBuilding = buildingService.findBy(id);
        return (optionalBuilding.isPresent()) ? new ResponseEntity<>(optionalBuilding.get(), HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/buildings")
    public ResponseEntity<Building> createOwner(@RequestBody Building building) {
        Building newBuilding = buildingService.save(building);
        return (newBuilding != null) ? new ResponseEntity<>(newBuilding, HttpStatus.CREATED) : new ResponseEntity<>(HttpStatus.CONFLICT);
    }

    @PutMapping("/buildings/{id}")
    public ResponseEntity<Building> updateOwner(@RequestBody Building building) {
        Building newBuilding = buildingService.update(building);
        return (newBuilding != null) ? new ResponseEntity<>(newBuilding, HttpStatus.OK) : new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
    }

    // BE CAREFUL - Delete a building
    @DeleteMapping("/buildings/{id}")
    public ResponseEntity<Void> delete(@PathVariable long id) {
        buildingService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
