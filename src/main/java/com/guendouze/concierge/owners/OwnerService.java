package com.guendouze.concierge.owners;

import com.guendouze.concierge.errors.ConflictException;
import com.guendouze.concierge.errors.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 29/01/2020 11:29.
 */

@Service
public class OwnerService {
    @Autowired
    OwnerRepository ownerRepository;

    public List<Owner> findAll() {
        List<Owner> owners = new ArrayList<>();
        ownerRepository.findAll().forEach(owners::add);
        return owners;
    }

    public Optional<Owner> findBy(long id) {
        Optional<Owner> optionalOwner = ownerRepository.findById(id);
        if (optionalOwner.isEmpty()) {
            throw new NotFoundException("owner not found");
        }
        return optionalOwner;
    }

    public Owner save(Owner owner) {
        if (owner.getId() != null && (ownerRepository.findById(owner.getId()) != null)) {
            throw new ConflictException("Another record with the same title exists");
        }
        return ownerRepository.save(owner);
    }

    public Owner update(Owner owner) {
        return ownerRepository.save(owner);
    }

    public void delete(long id) {
        ownerRepository.deleteById(id);
    }

}
