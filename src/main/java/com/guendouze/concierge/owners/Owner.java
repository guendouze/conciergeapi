package com.guendouze.concierge.owners;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 28/01/2020 14:09.
 */

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "owners")
public class Owner implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Civility is required")
    private String civility;

    @NotNull(message = "First name is required")
    @Size(min = 2, max = 25, message = "First name  must be between 2 and 25 characters")
    private String firstName;

    @NotNull(message = "Last name is required")
    @Size(min = 2, max = 25, message = "Last name must be between 2 and 25 characters")
    private String lastName;

    @NotNull(message = "Email is required")
    @Email(message = "Email should be valid")
    private String email;

    @NotNull(message = "Phone number is required")
    private String phoneNumber;

    @NotNull(message = "Number of street is required")
    private String number;

    @NotNull(message = "Street is required")
    private String street;

    @NotNull(message = "City is required")
    private String city;

    @NotNull(message = "Zip code is required")
    private String zipCode;

    public Owner(long id) {
        this.id = id;
    }
}
