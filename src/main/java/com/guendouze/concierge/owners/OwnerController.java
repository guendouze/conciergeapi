package com.guendouze.concierge.owners;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 29/01/2020 11:28.
 */

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = {"/api/v1/concierge"})
public class OwnerController {
    private final Log logger = LogFactory.getLog(OwnerController.class);

    @Autowired
    private OwnerService ownerService;

    @GetMapping("/owners")
    public ResponseEntity<List<Owner>> getAllOwners() {
        List<Owner> owners = ownerService.findAll();
        return new ResponseEntity<>(owners, HttpStatus.OK);
    }

    @GetMapping("/owners/{id}")
    public ResponseEntity<Owner> getOwner(@Valid @PathVariable long id) {
        Optional<Owner> optionalOwner = ownerService.findBy(id);
        return (optionalOwner.isPresent()) ? new ResponseEntity<>(optionalOwner.get(), HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/owners")
    public ResponseEntity<Owner> createOwner(@Valid @RequestBody Owner owner) {
        Owner newOwner = ownerService.save(owner);
        return (newOwner != null) ? new ResponseEntity<>(newOwner, HttpStatus.CREATED) : new ResponseEntity<>(HttpStatus.CONFLICT);
    }

    @PutMapping("/owners")
    public ResponseEntity<Owner> updateOwner(@RequestBody Owner owner) {
        Owner newOwner = ownerService.update(owner);
        return (newOwner != null) ? new ResponseEntity<>(newOwner, HttpStatus.OK) : new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping("/owners/{id}")
    public ResponseEntity<Void> deleteOwner(@PathVariable long id) {
        ownerService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
