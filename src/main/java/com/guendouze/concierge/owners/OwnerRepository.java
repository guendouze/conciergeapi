package com.guendouze.concierge.owners;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Nabil GUENDOUZE <guendouze@gmail.com>
 * @Created 29/01/2020 11:30.
 */

@Repository
public interface OwnerRepository extends JpaRepository<Owner, Long> {
}
